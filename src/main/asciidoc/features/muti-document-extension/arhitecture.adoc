// web (parse the url path to the service) and executes the authentication

ResourceController


// resource service loouup the request and and validates the authorization
ResourceService

- ResourceResolverService
- resolves the service and validates the authorization  for the resource
- ResourceGuard: validates the authorization
- ResourceHandlerService  - handles the action for the resource and the subresource it locates the implementing extension and pass the resource from storage
- ResourceStorage - gets the resource from storage and pass it to the ResourceDefinitionSpi handler
- List<ResourceDefinitionSpi> - the extension specific handlers for the action


[plantuml]
....
@startuml

    actor "ResourceAction" as ACT
    participant ResourceController as RC_WEB
    participant DomainGuard as RCG_WEB
    participant ResourceService as RS_SERVICE
    participant ResourceResolverService as RRS_SERVICE
    participant ResourceGuard as RG_SERVICE
    participant ResourceHandlerService as RH_SERVICE
    participant ResourceStorage as RST_SERVICE
    participant ResourceHaSpi as RH_SPI

    ACT ->>  RC_WEB: Get/Update/Delete/Create\nResource
    RC_WEB ->>  RCG_WEB: Authorized for domain action
    RCG_WEB -->>  RC_WEB: Authorized for domain action
    RC_WEB ->>  RS_SERVICE: Execute\n resource action
    RS_SERVICE ->>  RRS_SERVICE: Resolve And\n Authorize Request
    RRS_SERVICE ->>  RG_SERVICE: Authorize for \nAction (sub)resource
    RG_SERVICE -->>  RRS_SERVICE: Authorized
    RRS_SERVICE -->>  RS_SERVICE: Resolve And\n Authorize Request
    RS_SERVICE ->>  RH_SERVICE: Process resource action
    RH_SERVICE ->>  RST_SERVICE: Get Resource
    RST_SERVICE -->>  RH_SERVICE: return input stream
    RH_SERVICE ->>  RH_SPI: Handle by the extension
    RH_SPI -->>  RH_SERVICE: Write to output stream
    RH_SERVICE ->>  RST_SERVICE: Write
    RST_SERVICE -->>  RH_SERVICE: return input stream
    RH_SERVICE -->>  RS_SERVICE : Write to output stream
    RS_SERVICE -->>  RC_WEB : Write to output stream
    RC_WEB -->>  ACT: Response

@enduml
....
