= Redirection to AP service metadata

Access point service provider (iso6523-actorid-upis::9925:0367302178) is services for multiple users. The access point register service metadata data for the supported services to the
SMP

[source,xml]
----
<?xml version="1.0" encoding="UTF-8"?>
<ServiceGroup xmlns:smb="http://docs.oasis-open.org/bdxr/ns/SMP/2/BasicComponents"
              xmlns:sma="http://docs.oasis-open.org/bdxr/ns/SMP/2/AggregateComponents"
              xmlns="http://docs.oasis-open.org/bdxr/ns/SMP/2/ServiceGroup">
    <smb:SMPVersionID>2.0</smb:SMPVersionID>
    <smb:ParticipantID schemeID="iso6523-actorid-upis">9925:0367302178</smb:ParticipantID>
    <sma:ServiceReference>
        <smb:ID schemeID="bdx-docid-qns">urn::epsos:services##epsos-21</smb:ID>
        <sma:Process>
            <smb:ID schemeID="cenbii-procid-ubl">urn:www.cenbii.eu:profile:bii05:ver2.0</smb:ID>
        </sma:Process>
    </sma:ServiceReference>
    <sma:ServiceReference>
        <smb:ID schemeID="bdx-docid-qns">Invoice:ver2.0</smb:ID>
        <sma:Process>
            <smb:ID schemeID="cenbii-procid-ubl">urn:www.cenbii.eu:profile:bii05:ver2.0</smb:ID>
        </sma:Process>
    </sma:ServiceReference>
    <sma:ServiceReference>
        <smb:ID schemeID="bdx-docid-qns">Order:ver1.0</smb:ID>
        <sma:Process>
            <smb:ID schemeID="cenbii-procid-ubl">urn:www.cenbii.eu:profile:bii05:ver2.0</smb:ID>
        </sma:Process>
    </sma:ServiceReference>
    <sma:ServiceReference>
        <smb:ID schemeID="urn:eu:health:action">Insurance:ver1.0</smb:ID>
        <sma:Process>
            <smb:ID schemeID="urn:eu:health:service"></smb:ID>
        </sma:Process>
    </sma:ServiceReference>
</ServiceGroup>
----

The ServiceMetadata document for particular (Service/Action/Document)
[source,xml]
----
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<ServiceMetadata xmlns:smb="http://docs.oasis-open.org/bdxr/ns/SMP/2/BasicComponents"  xmlns:sma="http://docs.oasis-open.org/bdxr/ns/SMP/2/AggregateComponents" xmlns="http://docs.oasis-open.org/bdxr/ns/SMP/2/ServiceMetadata">
    <smb:SMPVersionID>2.0</smb:SMPVersionID>
    <smb:ParticipantID schemeID="iso6523-actorid-upis">9915:123456789</smb:ParticipantID>
    <smb:ServiceID schemeID="bdx-docid-qns">Invoice:ver2.0</smb:ServiceID>
    <sma:ProcessMetadata>
        <sma:Process>
            <smb:ID schemeID="cenbii-procid-ubl">urn:www.cenbii.eu:profile:bii05:ver2.0</smb:ID>
        </sma:Process>
        <sma:Endpoint>
            <smb:TransportProfileID>bdxr-transport-ebms3-as4-v1p0</smb:TransportProfileID>
            <smb:Description>contact@example.com</smb:Description>
            <smb:Contact>Access point for testing</smb:Contact>
            <smb:AddressURI>https://ap.example.com/as4</smb:AddressURI>
            <smb:ActivationDate>2018-04-12</smb:ActivationDate>
            <smb:ExpirationDate>2020-04-12</smb:ExpirationDate>
            <sma:Certificate>
                <smb:Subject>CN=EXAMPLE AP,C=NO</smb:Subject>
                <smb:Issuer>CN=EXAMPLE AP,C=NO</smb:Issuer>
                <smb:ActivationDate>2018-04-12</smb:ActivationDate>
                <smb:ExpirationDate>2025-04-12</smb:ExpirationDate>
                <smb:ContentBinaryObject mimeCode="application/base64">MIICwDCCAai..</smb:ContentBinaryObject>
            </sma:Certificate>
        </sma:Endpoint>
    </sma:ProcessMetadata>
</ServiceMetadata>
----


The users (or authorized AC service provider) register the redirection types


[source,xml]
----
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<ServiceMetadata xmlns:smb="http://docs.oasis-open.org/bdxr/ns/SMP/2/BasicComponents"
                 xmlns:sma="http://docs.oasis-open.org/bdxr/ns/SMP/2/AggregateComponents"
                 xmlns="http://docs.oasis-open.org/bdxr/ns/SMP/2/ServiceMetadata">
    <smb:SMPVersionID>2.0</smb:SMPVersionID>
    <smb:ParticipantID schemeID="iso6523-actorid-upis">0088:1124-endclient</smb:ParticipantID>
    <smb:ServiceID schemeID="bdx-docid-qns">Invoice:ver2.0</smb:ServiceID>
    <sma:ProcessMetadata>
        <sma:Process>
            <smb:ID schemeID="cenbii-procid-ubl">urn:www.cenbii.eu:profile:bii05:ver2.0</smb:ID>
        </sma:Process>
        <sma:Redirect>
            <smb:PublisherURI>https://ap.example.com/iso6523-actorid-upis::9915:123456789/services/bdx-docid-qns::Invoice:ver2.0</smb:PublisherURI>
        </sma:Redirect>
    </sma:ProcessMetadata>
</ServiceMetadata>

----
