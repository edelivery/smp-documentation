# SMP Documentation tools Docker Image


Purpose of the docker smp-doc image is to build the SMP documentation. The image contains all the necessary tools such as 
 - jdk 11 and maven: to execute the build
 - asciidoctor: to generate the documentation
 - graphviz: to generate diagrams
 - plantuml: to generate diagrams
 - ....

However, the image does not contain the source code of the SMP documentation. The source code of the SMP documentation 
is provided via mounting a volume with maven documentation project in to the docker container.

## Build docker image

Following command will build the docker image with the tag `edeliverytest/smp-builder:1.0`

    docker build --tag edeliverytestsmp-builder:1.0 .

## Run docker image

Following command will run the docker image with the tag `edelivery-docker.devops.tech.ec.europa.eu/edeliverytest/smp-doc:1.0` and mount the volume `/cef/documentation/smp-documentation` to `/data` in the container.

    docker run -v /cef/documentation/smp-documentation:/data edeliverytest/smp-builder:1.0

Parameters:
- `-v /cef/documentation/smp-documentation:/data` : mount the volume `/cef/documentation/smp-documentation` to `/data` in the container
- `edeliverytest/smp-builder:1.0` : tag of the docker image to run
the execution of the command will generate the documentation in the `/cef/documentation/smp-documentation/target/generated-docs` directory.
