# create builder to install maven which is needed
FROM eclipse-temurin:11-jdk as builder

ARG MAVEN_VERSION=3.9.6
ARG USER_HOME_DIR="/root"
ARG SHA=706f01b20dec0305a822ab614d51f32b07ee11d0218175e55450242e49d2156386483b506b3a4e8a03ac8611bae96395fd5eec15f50d3013d5deed6d1ee18224
ARG BASE_URL=https://downloads.apache.org/maven/maven-3/${MAVEN_VERSION}/binaries

ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"

RUN apt-get update \
  && apt-get install -y ca-certificates curl git gnupg dirmngr --no-install-recommends \
  && rm -rf /var/lib/apt/lists/*
RUN set -eux; curl -fsSLO --compressed ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${SHA} *apache-maven-${MAVEN_VERSION}-bin.tar.gz" | sha512sum -c - \
  && curl -fsSLO --compressed ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz.asc \
  && export GNUPGHOME="$(mktemp -d)"; \
  for key in \
  6A814B1F869C2BBEAB7CB7271A2A1C94BDE89688 \
  29BEA2A645F2D6CED7FB12E02B172E3E156466E8 \
  ; do \
  gpg --batch --keyserver hkps://keyserver.ubuntu.com --recv-keys "$key" ; \
  done; \
  gpg --batch --verify apache-maven-${MAVEN_VERSION}-bin.tar.gz.asc apache-maven-${MAVEN_VERSION}-bin.tar.gz
RUN mkdir -p ${MAVEN_HOME} ${MAVEN_HOME}/ref \
  && tar -xzf apache-maven-${MAVEN_VERSION}-bin.tar.gz -C ${MAVEN_HOME} --strip-components=1 \
  && ln -s ${MAVEN_HOME}/bin/mvn /usr/bin/mvn



FROM  ubuntu:23.10
LABEL authors="Joze Rihtarsic"


ARG BUILD_USER=buser
ARG BUILD_GROUP=${BUILD_USER}
ARG UID=1000
ARG GID=1000

#===================
# Timezone settings set to UTC by default!
#===================
ENV TZ "UTC"

ENV BUILD_USER=${BUILD_USER} \
    BUILD_GROUP=${BUILD_GROUP} \
    UID=${UID} \
    GID=${GID} \
    DATA=/data


ENV MAVEN_HOME /usr/share/maven
COPY --from=builder ${MAVEN_HOME} ${MAVEN_HOME}

RUN ln -s ${MAVEN_HOME}/bin/mvn /usr/bin/mvn
RUN apt-get update -qqy \
 && apt-get upgrade -qqy \
  && apt-get -qqy --no-install-recommends install  \
    asciidoctor \
    graphviz \
    tzdata  \
    curl \
    openjdk-11-jdk \
# install chrome for mmdc and puppeter \
 && apt-get -qqy --no-install-recommends install  \
   chromium-browser libnss3 libnspr4 libatk1.0-0 libatk-bridge2.0-0 libcups2 libdrm2 libxkbcommon0 libxcomposite1 libxdamage1 libxfixes3 libxrandr2 libgbm1 libasound2 # for puppeteer and memraid mmdc
# install latest nodejs
RUN curl -fsSL https://deb.nodesource.com/setup_21.x | bash - \
    &&  apt-get install -y nodejs \
    && rm -rf /var/lib/apt/lists/*

# create test user, folders and configure timezone
RUN usermod -u 1795 ubuntu \
  && groupmod -g 1795 ubuntu \
  && addgroup --gid=${GID}  ${BUILD_GROUP} \
  && useradd --create-home \
          --shell /bin/bash \
          -g ${BUILD_GROUP} \
          --uid ${UID} \
        ${BUILD_USER}
RUN mkdir -p "${DATA}" \
  && ln -fs /usr/share/zoneinfo/${TZ} /etc/localtime \
  && dpkg-reconfigure -f noninteractive tzdata \
  && cat /etc/timezone

USER ${BUILD_USER}
# install maven dependencies
COPY pom.xml /tmp/pom.xml
RUN mvn -f /tmp/pom.xml dependency:go-offline

WORKDIR /data


CMD ["mvn", "clean", "install"]
