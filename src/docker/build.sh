#!/usr/bin/env bash

# This is build script for building smp build image.

WORKING_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
cd "${WORKING_DIR}"

copyExternalImageResources() {
		echo "Copy test project resources ..."
		mkdir -p ./artefacts
     # copy pom for test and download maven dependencies in to the image
     ${WORKING_DIR}/../../pom.xml ./artefacts
}

cleanExternalImageResources() {
    echo "Clean external resources ..."
    [[ -d  ./artefacts/ ]] && rm -rf ./artefacts
}

composeBuildImage() {
	echo "Build ${IMAGE_NAME_DOMIBUS_SOAPUI} image..."
	docker compose -f docker-compose.build.yml build
}


# clean external resources before copy
copyExternalImageResources
composeBuildImage
cleanExternalImageResources
